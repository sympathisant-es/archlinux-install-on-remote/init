#!/usr/bin/env bash

set -e

script_dir=$(dirname $0)

for inventory_dir in inventory/host_vars/*
do
    SERVER_IP=$(cat "${inventory_dir}"/042_host.yml | grep ansible_host | cut -f2 -d' ')
    ssh-keygen -R ${SERVER_IP} -f $(pwd)/.ssh/known_hosts
    ssh-keyscan -H ${SERVER_IP} >> $(pwd)/.ssh/known_hosts
done
