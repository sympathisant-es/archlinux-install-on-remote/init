#!/usr/bin/env bash

set -e

script_dir=$(dirname $0)
source ${script_dir}/include/save_vault_file.sh
source ${script_dir}/include/define_inventory_path.sh # set SERVER_HOSTNAME
source ${script_dir}/include/ansible_env.sh

lock_file=$(pwd)/$(basename ${SERVER_HOSTNAME}.lock)

if [ -f "${lock_file}" ]
then
    echo "Already Run !"
    echo "Please Clean Inventory And Remove:" "${lock_file}"
    exit 42
fi

ansible-playbook -i inventory --ssh-common-args "-F $(pwd)/.ssh/config" --vault-id ${vault_password_file} --extra-vars "target=${SERVER_HOSTNAME}" ansible-playbooks/install_archlinux.yml

## After arch-bootstrap.sh installation root password should be root
## note also that the previous ovh_ssh_password defined is temporarie as we have boot on rescue disk
# not need as ssh key have been added
# ansible-vault encrypt_string 'root' --name 'ovh_ssh_password' --vault-id ${vault_password_file} >> ${vault_yml_file}
# replace by:
echo '---' > ${pass_yml_file}
echo "ansible_ssh_private_key_file: ${PWD}/.ssh/id_ed25519_ansible" >> ${pass_yml_file}


echo "You should reboot ${SERVER_HOSTNAME}"

touch "${lock_file}"
