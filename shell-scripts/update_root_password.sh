#!/usr/bin/env bash

set -e

script_dir=$(dirname $0)
source ${script_dir}/include/save_vault_file.sh
source ${script_dir}/include/ansible_env.sh

ansible-playbook -i inventory --ssh-common-args "-F $(pwd)/.ssh/config" --vault-id ${vault_password_file} ansible-playbooks/change_root_password.yml


### Disable as we now use ssh key to connect

#### always reset password file
###for inventory_dir in $(pwd)/inventory/host_vars/*
###do
###    if [ ! -f "${inventory_dir}"/999_user.yml ] # this file is created when root login is removed
###    then
###        pass_yml_file="${inventory_dir}"/999_pass.yml
###        echo '---' > ${pass_yml_file}
###        echo 'ansible_ssh_password: "{{ root_new_password }}"' >> ${pass_yml_file}
###    fi
###done
###
