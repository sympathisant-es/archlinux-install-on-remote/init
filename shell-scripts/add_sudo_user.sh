#!/usr/bin/env bash

set -e

script_dir=$(dirname $0)
source ${script_dir}/include/save_vault_file.sh
source ${script_dir}/include/ansible_env.sh

ansible-playbook -i inventory  --ssh-common-args "-F $(pwd)/.ssh/config" --vault-id ${vault_password_file} ansible-playbooks/create_sudo_user.yml


# always user file
for inventory_dir in $(pwd)/inventory/host_vars/*
do
    user_yml_file="${inventory_dir}"/999_user.yml
    echo '---' > ${user_yml_file}
    echo 'ansible_ssh_user: "{{ sudo_user }}"' >> ${user_yml_file}

    pass_yml_file="${inventory_dir}"/999_pass.yml
    echo '---' > ${pass_yml_file}
    echo 'ansible_become: yes' >> ${pass_yml_file}
    echo 'ansible_become_password: "{{ sudo_user_password }}"' >> ${pass_yml_file}
done
