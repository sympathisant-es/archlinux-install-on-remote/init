#!/usr/bin/env bash

set -e

script_dir=$(dirname $0)
source ${script_dir}/include/save_vault_file.sh
source ${script_dir}/include/define_inventory_path.sh # set SERVER_HOSTNAME

lock_file=$(pwd)/$(basename ${SERVER_HOSTNAME}.lock)

inventory_hosts=$(pwd)/inventory/hosts
is_in_inventory=$(cat ${inventory_hosts} | grep ${SERVER_HOSTNAME} | wc -l)
if [ ${is_in_inventory} -eq 0 ]
then
    echo ${SERVER_HOSTNAME} >> $inventory_hosts
fi

if [ ! -f ${vault_yml_file} ]
then
    echo '---' > ${vault_yml_file}

    while [ -z ${SERVER_OVH_PASSWORD} ]
    do
        read -s -p "${SERVER_HOSTNAME}: Ovh Password (received by mail) ? " SERVER_OVH_PASSWORD
        echo
    done

    ansible-vault encrypt_string ${SERVER_OVH_PASSWORD} --name 'ovh_ssh_password' --vault-id ${vault_password_file} >> ${vault_yml_file}

    while [ -z ${SERVER_NEW_PASSWORD} ]
    do
        read -s -p "${SERVER_HOSTNAME}: The New Root Password? " SERVER_NEW_PASSWORD
        echo
    done

    echo >> ${vault_yml_file}

    ansible-vault encrypt_string ${SERVER_NEW_PASSWORD} --name 'root_new_password' --vault-id ${vault_password_file} >> ${vault_yml_file}
fi

if [ ! -f ${user_yml_file} ]
then
    while [ -z ${SERVER_USER} ]
    do
        read -p "${SERVER_HOSTNAME}: SSH User [root] ? " SERVER_USER
        SERVER_USER=${SERVER_USER:-root}
    done

    echo '---' > ${user_yml_file}
    echo 'ansible_ssh_user: '${SERVER_USER} >> ${user_yml_file}
fi

if [ ! -f ${host_yml_file} ]
then
    cur_ip=$(host ${SERVER_HOSTNAME} | cut -f4 -d' ')
    while [ -z ${SERVER_IP} ]
    do
        #read -p "${SERVER_HOSTNAME}: Ip Adress [${cur_ip}] ? " SERVER_IP
        #SERVER_IP=${SERVER_IP-"$cur_ip"}
        SERVER_IP="$cur_ip"
    done

    echo '---' > ${host_yml_file}
    echo 'ansible_host: '${SERVER_IP} >> ${host_yml_file}

    ssh-keygen -R ${SERVER_IP} -f $(pwd)/.ssh/known_hosts
    ssh-keyscan -H ${SERVER_IP} >> $(pwd)/.ssh/known_hosts

fi

if [ ! -f ${pass_yml_file} ]
then
    echo '---' > ${pass_yml_file}
    echo 'ansible_ssh_password: "{{ ovh_ssh_password }}"' >> ${pass_yml_file}
fi

if [ ! -f ${sudo_yml_file} ]
then
    while [ -z ${SERVER_SUDO_USER} ]
    do
        read -p "${SERVER_HOSTNAME}: Sudo User [ansible] ? " SERVER_SUDO_USER
        SERVER_SUDO_USER=${SERVER_SUDO_USER:-ansible}
    done

    echo '---' > ${sudo_yml_file}
    echo 'sudo_user: '${SERVER_SUDO_USER} >> ${sudo_yml_file}
fi

if [ ! -f ${sudo_vault_yml_file} ]
then
    while [ -z ${SERVER_SUDO_PASSWORD} ]
    do
        read -s -p "${SERVER_HOSTNAME}: Sudo Password ? " SERVER_SUDO_PASSWORD
        echo
    done

    echo '---' > ${sudo_vault_yml_file}
    ansible-vault encrypt_string ${SERVER_SUDO_PASSWORD} --name 'sudo_user_password' --vault-id ${vault_password_file} > ${sudo_vault_yml_file}

fi

################
#### PACMAN ####
################

if [ -f "${lock_file}" ]
then
    echo "Already Run !"
    echo "Please Clean Inventory And Remove:" "${lock_file}"
    exit 42
fi

### Disable preparation on remote host as
### there is no more archlinux image for kimusfi or ovh

### ansible_raw_cmd="ansible ${SERVER_HOSTNAME} --ssh-common-args '-F $(pwd)/.ssh/config' --vault-id ${vault_password_file} -i inventory -m raw -a"
###
### #echo $ansible_raw_cmd
###
### eval ${ansible_raw_cmd} '"'"wget 'https://pkgbuild.com/~eschwartz/repo/x86_64-extracted/pacman-static' -O /usr/bin/pacman-static"'"'
###
### eval ${ansible_raw_cmd} '"'"chmod +x /usr/bin/pacman-static"'"'
###
### eval ${ansible_raw_cmd} '"'"pacman-static -Sc --noconfirm"'"'
###
### eval ${ansible_raw_cmd} '"'"pacman-static -Syyuu --noconfirm"'"'
###
### echo "You should reboot ${SERVER_HOSTNAME}"

### touch "${lock_file}"
