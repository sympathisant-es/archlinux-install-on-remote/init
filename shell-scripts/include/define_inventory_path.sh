if [ -n "$1" ]
then
    SERVER_HOSTNAME=$1
fi

while [ -z ${SERVER_HOSTNAME} ]
do
    read -p "Hostname ? " SERVER_HOSTNAME
done

host_var_dir=$(pwd)/inventory/host_vars/${SERVER_HOSTNAME}
mkdir -p $host_var_dir

vault_yml_file=${host_var_dir}/001_vault.yml
user_yml_file=${host_var_dir}/042_user.yml
host_yml_file=${host_var_dir}/042_host.yml
pass_yml_file=${host_var_dir}/999_pass.yml
sudo_yml_file=${host_var_dir}/042_sudo.yml
sudo_vault_yml_file=${host_var_dir}/001_sudo_vault.yml
