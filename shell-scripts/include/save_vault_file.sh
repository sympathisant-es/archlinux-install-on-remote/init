vault_password_file=$(pwd)/.vault_password

if [ ! -f $vault_password_file ]
then
    while [ -z ${SERVER_ANSIBLE_VAULT_PASSWORD} ]
    do
        read -s -p "Ansible Vault Password ? " SERVER_ANSIBLE_VAULT_PASSWORD
        echo
    done

    echo -n ${SERVER_ANSIBLE_VAULT_PASSWORD} > ${vault_password_file}
else
    echo ${vault_password_file} "already exist, remove it to update vault password"
fi
