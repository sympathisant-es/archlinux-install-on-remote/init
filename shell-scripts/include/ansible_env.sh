export ANSIBLE_STDOUT_CALLBACK="debug"
export ANSIBLE_CALLBACK_WHITELIST="debug,profile_tasks"
export ANSIBLE_PIPELINING="True"
export ANSIBLE_NOCOWS="True"
