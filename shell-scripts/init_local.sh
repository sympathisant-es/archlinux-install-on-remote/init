#!/usr/bin/env bash

set -e

lock_file=$(pwd)/$(basename $0.lock)

if [ -f "${lock_file}" ]
then
    echo "Already Run !"
    echo "Please Remove" "${lock_file}"
    exit 42
fi

################
#### PACMAN ####
################

pacman_cmd="sudo pacman --noconfirm"

# install ansible
${pacman_cmd} -S ansible ansible-lint molecule yamllint molecule-docker molecule-podman
# install sshpass to connect to host with interactive password
${pacman_cmd} -S sshpass
# install python-cryptography for ansible vault better performance
${pacman_cmd} -S python-cryptography
# install python-passlib for ansible password hashing
${pacman_cmd} -S python-passlib
# install for ansible postgres collection
${pacman_cmd} -S python-psycopg2
# install for ansible dig lookup
${pacman_cmd} -S python-dnspython
#############
#### SSH ####
#############

ssh_dir=$(pwd)/.ssh
if [ ! -d ${ssh_dir} ]
then
    mkdir -p ${ssh_dir}
    chmod 700 ${ssh_dir}
    ssh_key_file=${ssh_dir}/id_ed25519_ansible
    ssh-keygen -t ed25519 -f ${ssh_key_file}  -C "Ansible key" -N ""
    ssh_know_hosts=${ssh_dir}/known_hosts
    touch ${ssh_know_hosts}
    echo "
Host *
     PreferredAuthentications password,publickey
     IdentityFile ${ssh_key_file}
     UserKnownHostsFile ${ssh_know_hosts}" > ${ssh_dir}/config

fi

###################
#### INVENTORY ####
###################

mkdir -p inventory inventory/group_vars inventory/host_vars
touch  inventory/hosts

touch "${lock_file}"
