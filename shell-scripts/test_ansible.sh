#!/usr/bin/env bash

set -e

script_dir=$(dirname $0)

source ${script_dir}/include/save_vault_file.sh
source ${script_dir}/include/ansible_env.sh

ansible all -i inventory --ssh-common-args "-F $(pwd)/.ssh/config" -m setup --vault-id ${vault_password_file} -a 'gather_subset=!all'
