## Install Kimsufi Arch Host And Ansible Inventory

This repo is a set of tools to create an ansible ready inventory with kimusfi/ovhcloud server using Archlinux.

#### Create Ansible Tree

- Run init_local.sh

```
./shell-scripts/init_local.sh
```

It will install ansible and create the inventory directory.

It will also create a .ssh directory.

It assume you're on Archlinux, as it use pacman for install.

#### Add Server To Inventory

You have to boot on rescue disk on ovh web ui to have a temporary password by email.

_It is mandatory before run any script to add server._

- Run add_server.sh

```
./shell-scripts/add_server.sh
```

You will have to run it for each server you want to add in your ansible inventory.

It will ask you some information:
```
Ansible Vault Password ?
Hostname ? ns3110629.ip-87-98-216.eu
ns3110629.ip-87-98-216.eu: Ovh Password (received by mail) ?
ns3110629.ip-87-98-216.eu: The New Root Password?
ns3110629.ip-87-98-216.eu: SSH User [root] ?
ns3110629.ip-87-98-216.eu: Ip Adress ? 87.98.216.20
ns3110629.ip-87-98-216.eu: Sudo User [ansible] ?
ns3110629.ip-87-98-216.eu: Sudo Password ?
```

It will create a file to store your vault password. You should remove it after run as it is not crypted.

It will use the vault password to store root password crypted in inventory file.


- Remove a Server

Edit the file to remove the hostname:
```
inventory/hosts
```

- Re-add a Server

It may be usefull, if reboot failed.

You should have removed it from the inventory file.

And also remove the lock file and inventory variable:

```
rm ns3101177.ip-91-121-210.eu.lock
rm -r inventory/host_vars/ns3101177.ip-91-121-210.eu/
```

Then you can re-install it in Kimusfi Ui.

Then re-install it with script:
```
./shell-scripts/add_server.sh
```

#### Test Inventory

```
./shell-scripts/test_ansible.sh
```

It should show some ansible var for each host added.

It should not be needed, but if you have some error with ssh key, you can update them with:
```
./shell-scripts/regen_know_hosts.sh
```

#### Install Archlinux

```
./shell-scripts/install_archlinux.sh
```

It will install Archlinux from the debian rescue disk provided by ovh.

Using https://github.com/tokland/arch-bootstrap

If installation is successfull, then you have to log on ovh ui and reboot on disk.

After Reboot run, :
```
./shell-scripts/regen_know_hosts.sh
```

__The root password will be set to 'root' so you have to quickly run the next step.__

#### Update root Password

To set the root password as defined when server have been added:
```
./shell-scripts/update_root_password.sh
```

#### Test Again Inventory

```
./shell-scripts/test_ansible.sh
```

#### Add Sudo User

To create the sudo user as defined when server have been added:
```
./shell-scripts/add_sudo_user.sh
```

It will also disable ssh password and disable root login


#### Test Again Inventory

```
./shell-scripts/test_ansible.sh
```

#### Reboot on Ovh/Kimusfi interface

You should reboot your server after installation, connect with ssh and check if every thing is fine
