for i in ../Air/roles/*
do
    if [ -d "$i" ]
       then
           echo "- src: $i"
           echo "  name: air."$(basename $i)
           echo "  scm: git"
    fi
done
